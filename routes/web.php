<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\loginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require 'login/login.php';
require 'admin/admin.php';
require 'inventarisasi-hukum/inventarisasiHukum.php';
require 'home/home.php';
require 'artikel/artikel.php';
require 'pencarian/pencarian.php';


Route::get('/login', [loginController::class, 'index'])->name('login');
