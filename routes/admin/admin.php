<?php

use App\Http\Controllers\adminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\admin\FormulaController;

Route::group(
    [
        'prefix'        => 'admin',
        'namespace'     => 'admin.',
        'as'            => 'admin.',
        'middleware' => ['web','auth']
    ],

    function () {
        //LOGIN
        Route::get('/dashboard', [adminController::class, 'index'])->name('dashboard');
        Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
        Route::get('/master-formula', [FormulaController::class, 'formula'])->name('master.formula');
        Route::get('/master-formula/tambah/{id}', [FormulaController::class, 'add'])->name('master.formula.add');
        Route::get('/master-kategori-update/{id}', [KategoriController::class, 'update'])->name('master.kategori.update');




    }
);
