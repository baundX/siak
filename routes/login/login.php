<?php

use App\Http\Controllers\loginController;

Route::group(
    [
        'prefix'        => 'login',
        'namespace'     => 'login',
        'as'            => 'login.',
        'middleware' => ['web']
    ],
    function () {
        //LOGIN
        Route::get('', [loginController::class, 'index'])->name('login');
        Route::post('/login-proses', [loginController::class, 'login_proses'])->name('proses');
    }
);
