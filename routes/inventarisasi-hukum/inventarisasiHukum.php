<?php

use App\Http\Controllers\InventarisasiHukumController;

Route::group(
    [
        'prefix'        => 'inventarisasi-hukum',
        'namespace'     => 'inventarisasi-hukum',
        'as'            => 'inventarisasi-hukum.',
        'middleware' => ['web']
    ],
    function () {
        //
        Route::get('', [InventarisasiHukumController::class, 'index'])->name('index');
        Route::get('detail/{id}', [InventarisasiHukumController::class, 'detail'])->name('detail');
        Route::get('kategori/{param}', [InventarisasiHukumController::class, 'kategori'])->name('kategori');
    }
);
