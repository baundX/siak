<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventarisasiHukum extends Model
{
    protected $table = 'inventarisasi_hukum';
    public $timestamps = false;
}
