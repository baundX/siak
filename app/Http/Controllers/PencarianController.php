<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class PencarianController extends Controller
{
    public function index()
    {
        $dokumen        = $_GET['dokumen'];
        $kategori       = $_GET['kategori'];
        $tahun          = $_GET['tahun'];
        $nomor          = $_GET['nomor'];

        $kategorilist = DB::table("kategori")->get();

        $pdf = DB::table('inventarisasi_hukum');
        $pdf->leftJoin('kategori', 'kategori.id', '=', 'inventarisasi_hukum.jenis');
        if($dokumen != ''){
            $pdf->where('content', 'like', '%'.$dokumen.'%');
        }
        if($kategori != ''){
            $pdf->where('kategori.link', 'like', '%'.$kategori.'%');
        }
        if($tahun != ''){
            $pdf->where('tahun_diundang',$tahun);
        }
        if($nomor != ''){
            $pdf->where('no_peraturan',$nomor);
        }
        $pdf->select(['kategori.nama','inventarisasi_hukum.*']);
        $data = $pdf->paginate(10);
        return view('page.pencarian.index', compact('data','dokumen','kategori','tahun','nomor','kategorilist'));
    }
}
