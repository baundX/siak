<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class FormulaController extends Controller
{
    public function formula()
    {
        return view('admin.page.formula.index');
    }
    public function add($id)
    {
        return view('admin.page.formula.add-'.$id);
    }
    public function datatable(Request $request)
    {
        $model = DB::table('kategori');
        $counter = clone $model;
        $counter->count();
        return DataTables::query($model)
            ->addColumn('action', function ($data) {
                $html = '<a href="#" class="btn btn-sm btn-danger hapuskategori" style="margin:5px;" id="'.$data->id.'">Hapus </a>';
                $html .= '<a href="#" class="btn btn-sm btn-success editkategori" style="margin:5px;" id="'.$data->id.'" data-bs-toggle="modal" data-bs-target="#kt_modal_create_app2">Edit </a>';
                return $html;
                })
            ->addIndexColumn()
            ->setTotalRecords($counter)
            ->toJson();
    }
}
