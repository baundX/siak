<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;

class Filecontroller extends Controller
{
    public function index()
    {
        return view('admin.page.file');
    }

    public function datatable(Request $request)
    {
        $model = DB::table('inventarisasi_hukum');
        $counter = clone $model;
        $counter->count();
        return DataTables::query($model)
            ->addColumn('action', function ($data) {
                $html = '<a href="#" class="btn btn-sm btn-danger" style="margin:5px;" id="hapurfile">Hapus </a>';
                $html .= '<a href="#" class="btn btn-sm btn-success" style="margin:5px;" id="editfile">Edit </a>';
                $html .= '<a href="#" class="btn btn-sm btn-primary" style="margin:5px;" id="detailfile">Detail </a>';
                return $html;
                })
            ->addIndexColumn()
            ->setTotalRecords($counter)
            ->toJson();
    }
}
