<?php

namespace App\Http\Controllers;

use App\Models\Berita;
use App\Models\InventarisasiHukum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        $data = Berita::select(['berita.*'])
                ->orderBy('tgl', 'DESC')
                ->paginate(4);
        return view('index',compact('data'));
    }

    public function pencarian($dokumen=null,$kategori=null,$tahun=null,$nomor=null)
    {
        // return view('pencarian');
    }

    public function detail($id)
    {
        $id = decrypt($id);
        $data = Berita::select('*')
                ->where('id', $id)->get();
        $updatedetail = Berita::where('id', $id);
        $updatedetail->update([
            'views' => $data[0]->views + 1
        ]);
        return view('page/berita/detail', compact('data'));
    }
}
