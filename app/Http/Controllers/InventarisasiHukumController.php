<?php

namespace App\Http\Controllers;

use App\Models\InventarisasiHukum;
use App\Models\Kategori;
use App\Services\InventarisasiHukumService;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Illuminate\Support\Facades\DB;

class InventarisasiHukumController extends Controller
{

    public function __construct(
        InventarisasiHukumService $InventarisasiHukumService
    ) {
        $this->service = $InventarisasiHukumService;
    }

    public function index()
    {
        $data = InventarisasiHukum::select(['kategori.nama','inventarisasi_hukum.*'])
            ->leftJoin('kategori', 'kategori.id', '=', 'inventarisasi_hukum.jenis')
            ->paginate(10);
        $title = 'Inventarisasi Hukum';
        $kategori = DB::table("kategori")->get();
        return view('page/inventarisasi-hukum/index', compact('data', 'title', 'kategori'));
    }

    public function detail($id)
    {
        $id = decrypt($id);
        $data = $this->service->detailInventarisasiHukum($id);
        $updatedetail = InventarisasiHukum::where('id', $id);
        $updatedetail->update([
            'view' => $data[0]->view + 1
        ]);
        return view('page/inventarisasi-hukum/detail', compact('data'));
    }

    public function kategori($param)
    {
        $data = InventarisasiHukum::select(['kategori.nama','inventarisasi_hukum.*'])
            ->leftJoin('kategori', 'kategori.id', '=', 'inventarisasi_hukum.jenis')
            ->where('kategori.link', $param)->paginate(10);
        $title = str_replace('-', ' ', $param);
        $kategori = DB::table("kategori")->get();
        return view('page/inventarisasi-hukum/index', compact('data', 'title', 'kategori'));
    }


}
