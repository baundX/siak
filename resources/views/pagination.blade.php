<!-- Previous page symbol "<" Link -->
@if ($paginator->hasPages())
<div class="d-flex flex-center mb-0">
    @if ($paginator->onFirstPage())
        <a href="#" class="css-w9xr1c-norm"><i class="fa fa-angle-double-left"></i></a>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" class="css-w9xr1c-norm"><i class="fa fa-angle-double-left"></i></a>
    @endif

    @foreach ($elements as $element)
            {{-- "2 Dots" Separator --}}
            @if (is_string($element))
                <span class="btn btn-icon h-30px w-30px fw-semibold">{{ $element }}</span>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a href="#" class="css-w9xr1c-norm">{{ $page }}</a>
                    @else
                        <a href="{{ $url }}" class="btn btn-icon h-30px w-30px fw-semibold">{{ $page }}</a>

                    @endif
                @endforeach
            @endif
    @endforeach
    {{-- Next Page symbol ">" Link --}}
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" class="css-w9xr1c-norm"><i class="fa fa-angle-double-right"></i></a>
    @else
        <a href="#" class="css-w9xr1c-norm"><i class="fa fa-angle-double-right"></i></a>
    @endif
</div>
@endif

