<script>
"use strict";
    var KTSigninGeneral=function(){
        var t,e,i,email,password;
        return{
            init:function(){
                t=document.querySelector("#kt_sign_in_form"),
                e=document.querySelector("#kt_sign_in_submit"),
                i=FormValidation.formValidation(t,{
                    fields:{email:{validators:{notEmpty:{message:"Harap isi kolom ini"},
                    emailAddress:{message:"Harap isi format email dengan benar"}}},
                    password:{validators:{notEmpty:{message:"Harap isi kolom ini"}}}},
                    plugins:{trigger:new FormValidation.plugins.Trigger,bootstrap:new FormValidation.plugins.Bootstrap5({rowSelector:".fv-row"})}
                }),
                    e.addEventListener("click",(function(n){
                        n.preventDefault(),i.validate().then((function(i){
                            "Valid"==i?(e.setAttribute("data-kt-indicator","on"),e.disabled=!0,setTimeout((
                            function(){
                            e.removeAttribute("data-kt-indicator"),e.disabled=!1,
                            email = $('#email').val()
                            password = $('#password').val()
                            console.log(email,password)
                            $.ajax({
                                url: "{{ route('login.proses') }}",
                                type: 'POST',
                                dataType: "JSON",
                                timeout: 10000,
                                data: {
                                    _token: "{{ csrf_token() }}",
                                    email: email,
                                    password: password
                                },
                                success: function (data) {
                                    if (data.status=='success') {
                                        Swal.fire({
                                            title: data.msg,
                                            timer: 1500,
                                            timerProgressBar: true,
                                            didOpen: () => {
                                                Swal.showLoading()
                                                const b = Swal.getHtmlContainer().querySelector('b')
                                                timerInterval = setInterval(() => {
                                                b.textContent = Swal.getTimerLeft()
                                                }, 100)
                                            },
                                            willClose: () => {
                                                location.href = "{{ route('admin.dashboard') }}"
                                            }
                                            })
                                    } else {
                                        Swal.fire({
                                        text:data.msg,
                                        icon:"error",buttonsStyling:!1,confirmButtonText:"Ok",customClass:{confirmButton:"btn btn-primary"}
                                    })
                                    }
                                }, error: function () {
                                    Swal.fire({
                                        text:"Username atau password yang anda masukan tidak sesuai, Silahkan coba lagi !",
                                        icon:"error",buttonsStyling:!1,confirmButtonText:"Ok",customClass:{confirmButton:"btn btn-primary"}
                                    })
                                }
                            })
                            }),2e3)):Swal.fire({
                                        text:"Username atau password yang anda masukan tidak sesuai, Silahkan coba lagi !",
                                        icon:"error",buttonsStyling:!1,confirmButtonText:"Ok",customClass:{confirmButton:"btn btn-primary"}
                                    })
                        }))
                    }))
                }
            }
    }();
KTUtil.onDOMContentLoaded((function(){KTSigninGeneral.init()}));
</script>