        <title>SIAK</title>
		<meta charset="utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

		<!-- <link rel="canonical" href="https://preview.keenthemes.com/metronic8" /> -->

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

		<link href="{{ asset('plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />

		<link href="{{ asset('plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <style>
        body {
            background-color: #f9fafb !important;
        }
        .white {
            color:white;
        }
        .brand {
            --tw-bg-opacity: 1;
            background-color: rgb(27, 40, 63);
            -webkit-box-shadow: none;
            color:white;
            box-shadow: none;

        }
        .brand-logo {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: flex-begin;
            -ms-flex-pack: flex-begin;
            justify-content: flex-begin;
        }
        .menu {
            background-color: #ffffff;
            -webkit-box-shadow: none;
            color:#000000;
            box-shadow: none;
        }
        .max-h-20px {
            max-height: 20px !important;
        }
        .max-h-30px {
            max-height: 30px !important;
        }
        .font-size-h6 {
            font-size: 1.175rem !important;
        }
        .pl-2, .px-2 {
            padding-left: 0.5rem !important;
        }
        .sm\:text-left {
            text-align: left;
        }
        </style>
@include('admin.partial.topbar')
