@extends('app-admin')
@section('head')
    @include('admin.partial.head')
@endsection
@section('content')
<!--begin::Wrapper-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Toolbar-->
						<div class="toolbar" id="kt_toolbar">
							<!--begin::Container-->
							<div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
								<!--begin::Page title-->
								<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
									<!--begin::Title-->
									<h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">Data Master
									<!--begin::Separator-->
									<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
									<!--end::Separator-->
									<!--end::Description--></h1>
									<!--end::Title-->
								</div>
								<!--end::Page title-->
								<!--begin::Actions-->
								<div class="d-flex align-items-center gap-2 gap-lg-3">
								</div>
								<!--end::Actions-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Toolbar-->
						<!--begin::Post-->
						<div class="post d-flex flex-column-fluid" id="kt_post">
							<!--begin::Container-->
							<div id="kt_content_container" class="container-xxl">
                            <div class="row">
                                <div class="card card-flush h-xl-100">
                                    <div class="card-header">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="card-label fw-bolder text-gray-800">Data Formula A1</span>
                                        </h3>
                                        <div class="card-title align-items-end flex-column">
                                            <!-- <a href="#" class="btn btn-dark" style="background-color:red;"><i class="fa fa-plus"></i>Tambah Baru</a> -->
                                        </div>
                                    </div>
                                    <div class="card-body pt-2">
                                        <table class="table table-bordered table-hover w-100" id="datatables">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Aldeia</th>
													<th>Suku</th>
													<th>Postu Administrativu</th>
													<th>Munisipiu</th>
													<th>Naran</th>
													<th>Naran Istimadu</th>
													<th>Tinan</th>
													<th>Seksu</th>
													<th>Estatu Labarik</th>
													<th>Profisaum Labarik</th>
													<th>Aksi</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
							</div>
							<!--end::Container-->
						</div>
						<!--end::Post-->
					</div>
					<!--end::Content-->

{{--@include('admin.partial.footer')--}}
@endsection
@section('script')
    @include('admin.partial.script')
    <script>
       $('#datatables').DataTable({
            responsive: true,
            processing: true,
            serverSide: false,
            ajax: {
                url: '{{ route('admin.master.file.datatable') }}',
                type: 'post',
				data: { "_token": "{{ csrf_token() }}" }
            },
            columns: [
                {data: 'DT_RowIndex', width: '5%', orderable: false, searchable: false},
                {data: 'singkatan_jenis'},
                {data: 'no_peraturan'},
                {data: 'pengarang'},
                {data: 'tgl_ditetap'},
                {data: 'tgl_diundang'},
                {data: 'tahun_diundang'},
                {data: 'content'},
                {data: 'tmp_terbit'},
                {data: 'penerbit'},
                {data: 'sumber'},
                {data: 'action'}
            ],
            order: [[1,'asc']]
        });
    </script>
@endsection

