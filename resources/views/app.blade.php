<!DOCTYPE html>
<html lang="en">
<head>
@yield('head')
</head>
<body data-kt-name="metronic" id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled">
@yield('content')
</body>
@yield('footer')
</html>
