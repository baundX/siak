<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
    <span class="svg-icon">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="currentColor" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="currentColor" />
				</svg>
			</span>
</div>
<div class="pt-xl-15">
    <div class="card rounded-0 bgi-no-repeat bgi-position-x-end bgi-size-cover" style="background-color: rgb(27, 40, 63);">
        <div class="card-body container-xxl pt-10 pb-8 mt-20">
            <div class="grid grid-cols-8 px-8 sm:p-14 font-karla bg-red-700 text-white">
                <div class="col-span-8 sm:col-span-2 ml-2">
                    <a href="{{ url('') }}">
                        <div class="mb-3 flex">
                            <img src="{{ asset('media/svg/jdih-jatengv3.svg') }}" alt="Logo JDIH" class="h-8 bg-contain">
                        </div>
                    </a>
                    <a href="https://diskominfo.jatengprov.go.id" target="_blank">
                        <div class="mt-5 flex">
                            <img src="{{ asset('media/svg/logo-jawa-tengah.svg') }}" alt="Logo Diskominfo" class="w-16 inline-block">
                            <p class="text-l text-white ml-2 self-center"><br>Biro Hukum <br>Provinsi Jawa Tengah</p>
                        </div>
                    </a>
                </div>
                <div class="col-span-8 sm:col-span-2 ml-2 d-flex justify-content-lg-start">
                    <div class="pb-10">
                        <h3 class="text-white mb-5 mb-lg-7 font-size-4">SOSIAL MEDIA</h3>
                        <div class="d-flex flex-column font-size-3 font-weight-bold">
                            <a href="https://www.youtube.com/channel/UCFFZo4PAotie8YCr49chvqg" target="_blank">
                                <div class="mt-3 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 512 512">
                                        <path d="M352 256C352 278.2 350.8 299.6 348.7 320H163.3C161.2 299.6 159.1 278.2 159.1 256C159.1 233.8 161.2 212.4 163.3 192H348.7C350.8 212.4 352 233.8 352 256zM503.9 192C509.2 212.5 512 233.9 512 256C512 278.1 509.2 299.5 503.9 320H380.8C382.9 299.4 384 277.1 384 256C384 234 382.9 212.6 380.8 192H503.9zM493.4 160H376.7C366.7 96.14 346.9 42.62 321.4 8.442C399.8 29.09 463.4 85.94 493.4 160zM344.3 160H167.7C173.8 123.6 183.2 91.38 194.7 65.35C205.2 41.74 216.9 24.61 228.2 13.81C239.4 3.178 248.7 0 256 0C263.3 0 272.6 3.178 283.8 13.81C295.1 24.61 306.8 41.74 317.3 65.35C328.8 91.38 338.2 123.6 344.3 160H344.3zM18.61 160C48.59 85.94 112.2 29.09 190.6 8.442C165.1 42.62 145.3 96.14 135.3 160H18.61zM131.2 192C129.1 212.6 127.1 234 127.1 256C127.1 277.1 129.1 299.4 131.2 320H8.065C2.8 299.5 0 278.1 0 256C0 233.9 2.8 212.5 8.065 192H131.2zM194.7 446.6C183.2 420.6 173.8 388.4 167.7 352H344.3C338.2 388.4 328.8 420.6 317.3 446.6C306.8 470.3 295.1 487.4 283.8 498.2C272.6 508.8 263.3 512 255.1 512C248.7 512 239.4 508.8 228.2 498.2C216.9 487.4 205.2 470.3 194.7 446.6H194.7zM190.6 503.6C112.2 482.9 48.59 426.1 18.61 352H135.3C145.3 415.9 165.1 469.4 190.6 503.6V503.6zM321.4 503.6C346.9 469.4 366.7 415.9 376.7 352H493.4C463.4 426.1 399.8 482.9 321.4 503.6V503.6z"></path>
                                    </svg>
                                    <span class="ml-2 text-white">JDIH Provinsi Jawa Tengah</span>
                                </div>
                            </a>
                            <a href="https://www.facebook.com/jdihprovjateng" target="_blank">
                                <div class="mt-3 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 448 512">
                                        <!--! Font Awesome Pro 6.1.1 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                        <path d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"></path>
                                    </svg>
                                    <span class="ml-2 text-white">JDIH Provinsi Jawa Tengah</span>
                                </div>
                            </a>
                            <a href="https://www.instagram.com/jdihprovjateng/" target="_blank">
                                <div class="mt-3 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 448 512">
                                        <path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                                    </svg>
                                    <span class="ml-2 text-white">@jdihprovjateng</span>
                                </div>
                            </a>
                            <a href="https://twitter.com/jdihprovjateng" target="_blank">
                                <div class="mt-3 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 512 512">
                                        <!--! Font Awesome Pro 6.1.1 by @fontawesome  - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. -->
                                        <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                                    </svg>
                                    <span class="ml-2 text-white">@jdihprovjateng</span>
                                </div>
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.smt.pelitajateng2" target="_blank">
                                <div class="mt-6 flex">
                                    <img src="{{ asset('media/svg/playstore.svg') }}" class="mh-30px mh-lg-40px loaded" alt="">

                                    <span class="ml-2 text-white">&nbsp;</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-span-8 sm:col-span-2 ml-2 d-flex justify-content-lg-start">
                    <div class="pb-10">
                        <h3 class="text-white mb-5 mb-lg-7 font-size-4">KONTAK KAMI</h3>
                        <div class="d-flex flex-column font-size-3 font-weight-bold">
                            <a href="{{ url('') }}" target="_blank">
                                <div class="mt-3 flex">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 512 512">
                                        <path d="M352 256C352 278.2 350.8 299.6 348.7 320H163.3C161.2 299.6 159.1 278.2 159.1 256C159.1 233.8 161.2 212.4 163.3 192H348.7C350.8 212.4 352 233.8 352 256zM503.9 192C509.2 212.5 512 233.9 512 256C512 278.1 509.2 299.5 503.9 320H380.8C382.9 299.4 384 277.1 384 256C384 234 382.9 212.6 380.8 192H503.9zM493.4 160H376.7C366.7 96.14 346.9 42.62 321.4 8.442C399.8 29.09 463.4 85.94 493.4 160zM344.3 160H167.7C173.8 123.6 183.2 91.38 194.7 65.35C205.2 41.74 216.9 24.61 228.2 13.81C239.4 3.178 248.7 0 256 0C263.3 0 272.6 3.178 283.8 13.81C295.1 24.61 306.8 41.74 317.3 65.35C328.8 91.38 338.2 123.6 344.3 160H344.3zM18.61 160C48.59 85.94 112.2 29.09 190.6 8.442C165.1 42.62 145.3 96.14 135.3 160H18.61zM131.2 192C129.1 212.6 127.1 234 127.1 256C127.1 277.1 129.1 299.4 131.2 320H8.065C2.8 299.5 0 278.1 0 256C0 233.9 2.8 212.5 8.065 192H131.2zM194.7 446.6C183.2 420.6 173.8 388.4 167.7 352H344.3C338.2 388.4 328.8 420.6 317.3 446.6C306.8 470.3 295.1 487.4 283.8 498.2C272.6 508.8 263.3 512 255.1 512C248.7 512 239.4 508.8 228.2 498.2C216.9 487.4 205.2 470.3 194.7 446.6H194.7zM190.6 503.6C112.2 482.9 48.59 426.1 18.61 352H135.3C145.3 415.9 165.1 469.4 190.6 503.6V503.6zM321.4 503.6C346.9 469.4 366.7 415.9 376.7 352H493.4C463.4 426.1 399.8 482.9 321.4 503.6V503.6z"></path>
                                    </svg>
                                    <span class="ml-2 text-white">jdih.jatengprov.go.id</span>
                                </div>
                            </a>
                            <div class="mt-3 flex">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-4 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 512 512">
                                    <path d="M511.2 387l-23.25 100.8c-3.266 14.25-15.79 24.22-30.46 24.22C205.2 512 0 306.8 0 54.5c0-14.66 9.969-27.2 24.22-30.45l100.8-23.25C139.7-2.602 154.7 5.018 160.8 18.92l46.52 108.5c5.438 12.78 1.77 27.67-8.98 36.45L144.5 207.1c33.98 69.22 90.26 125.5 159.5 159.5l44.08-53.8c8.688-10.78 23.69-14.51 36.47-8.975l108.5 46.51C506.1 357.2 514.6 372.4 511.2 387z"></path>
                                </svg>
                                <span class="ml-2 text-white">(024) 8311282</span>
                            </div>
                            <div class="mt-3 flex">
                                <svg xmlns="http://www.w3.org/2000/svg" class="w-6 inline-block" fill="#ffff" stroke="currentColor" stroke-width="2" viewBox="0 0 576 512">
                                    <path d="M273.2 311.1C241.1 271.9 167.1 174.6 167.1 120C167.1 53.73 221.7 0 287.1 0C354.3 0 408 53.73 408 120C408 174.6 334.9 271.9 302.8 311.1C295.1 321.6 280.9 321.6 273.2 311.1V311.1zM416 503V200.4C419.5 193.5 422.7 186.7 425.6 179.8C426.1 178.6 426.6 177.4 427.1 176.1L543.1 129.7C558.9 123.4 576 135 576 152V422.8C576 432.6 570 441.4 560.9 445.1L416 503zM15.09 187.3L137.6 138.3C140 152.5 144.9 166.6 150.4 179.8C153.3 186.7 156.5 193.5 160 200.4V451.8L32.91 502.7C17.15 508.1 0 497.4 0 480.4V209.6C0 199.8 5.975 190.1 15.09 187.3H15.09zM384 504.3L191.1 449.4V255C212.5 286.3 234.3 314.6 248.2 331.1C268.7 357.6 307.3 357.6 327.8 331.1C341.7 314.6 363.5 286.3 384 255L384 504.3z"></path>
                                </svg>
                                <span class="ml-2 text-white">Gedung A Lantai 5, Jl. Pahlawan No.9 Semarang Jawa Tengah </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-span-8 sm:col-span-2 ml-2 d-flex justify-content-lg-start">
                    <div class="pb-10">
                        <h3 class="text-white mb-5 mb-lg-7 font-size-4">TAUTAN</h3>
                        <div class="d-flex flex-column font-size-3 font-weight-bold">
                            <a href="https://www.jdihn.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">jdihn.go.id</span>
                                </div>
                            </a>
                            <a href="https://www.setneg.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">setneg.go.id</span>
                                </div>
                            </a>
                            <a href="https://www.bphn.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">bphn.go.id</span>
                                </div>
                            </a>
                            <a href="https://jatengprov.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">jatengprov.go.id</span>
                                </div>
                            </a>
                            <a href="http://jdih.dprd.jatengprov.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">jdih.dprd.jatengprov.go.id</span>
                                </div>
                            </a>
                            <a href="https://www.kemendagri.go.id/" target="_blank">
                                <div class="mt-3 flex">
                                    <span class="ml-2 text-white">kemendagri.go.id</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
