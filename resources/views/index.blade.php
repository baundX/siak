@extends('app')
@section('head')
    @include('partial.head')
    <style>
        .select2-container .select2-selection--single {
            height: auto !important;
        }
    </style>
@endsection
@section('content')
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FS8GGP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<div class="d-flex flex-column flex-root">
    <div class="page d-flex flex-row flex-column-fluid">
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            @include('partial.topbar')
            <div class="card rounded-0 bgi-no-repeat bgi-position-x-end bgi-size-cover" style="background-color: #1B283F;background-size: auto 100%; background-image: url('https://preview.keenthemes.com/metronic8/demo16/assets/media/svg/general/rhone.svg')">

                <div class="container-xxl pt-10 pb-8 mx-auto col-md-10">
                    <div class="col-span-3 flex items-center justify-center">
                        <p class="text-left font-rubik">
                            <span class="text-xl text-white font-light font-poppins">Timor Leste</span><br>
                            <span class="sm:text-3xl text-white font-semibold">R e p r o d u s a u n</span>
                        </p>
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-lg-flex align-lg-items-center">
                            <div class="css-16gywht-pbxc rounded d-flex flex-column flex-lg-row align-items-lg-center bg-body p-5 w-xxl-750px h-lg-60px me-lg-10 my-5">
                                <div class="row flex-grow-1 mb-5 mb-lg-0">
                                    <div class="col col-lg-12 d-flex align-items-center mb-3 mb-lg-0 search-box">
                                        <input type="text" class="mr-2 form-control-search form-control-flush flex-grow-1" id="nama_dokumen" value="" placeholder="Search">
                                    </div>
                                </div>
                                <div class="min-w-150px search-box">
                                    <button type="submit" class="btn btn-searchbox fw-bolder" id="kt_advanced_search_button_1">
                                        <i class="bi bi-search"></i>Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content d-flex flex-column flex-column-fluid mx-auto col-md-10 pt-20" id="kt_content">
                <div id="kt_content_container" class="container-xxl">
                    <div class="row gy-5 g-xl-8">
                        <div class="col-xxl-12">
                            <div class="card css-16gydcw h-xl-100">
                                <div class="blockquote pt-10 text-center">
                                    <p class="mb-0">Surf with Us.</p>
                                    <h3 class="mb-0 text-uppercase pt-10 display-2-wx">The biggest browser on earth</h3>
                                </div>
                                <div class="row card-body py-8 mx-auto col-md-9">
                                    <div class="col-xxl-4 pb-8">
                                        <div class="card css-16gycmq-wht">
                                            <div class="blockquote px-2 py-2">
                                                <h3 class="mb-0 display-2-normal">1 Million +</h3>
                                                <p class="mb-0">user per day</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-4 pb-8">
                                        <div class="card css-16gycmq-wht">
                                            <div class="blockquote px-2 py-2">
                                                <h3 class="mb-0 display-2-normal">1000 +</h3>
                                                <p class="mb-0">extension</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xxl-4 pb-8">
                                        <div class="card css-16gycmq-wht">
                                            <div class="blockquote px-2 py-2">
                                                <h3 class="mb-0 display-2-normal">VPN</h3>
                                                <p class="mb-0">free</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12">
                            <div class="card css-16f7f8fc h-xl-100">
                                <div class="blockquote pt-6 pl-6">
                                    <div class="header-logo me-5 me-md-10 pb-5 flex-grow-1 flex-lg-grow-0">
                                        <h3 class="mb-0 text-uppercase display-2-wx">What Makes Vivaldo Different?</h3>
                                    </div>
                                </div>
                                <div class="separator mb-9 text-black"></div>
                                <div class="row card-body">
                                    <div class="col-3">
                                        <div>
                                            <div class="px-2 py-2">

                                                <h3 class="mb-0 sm:text-2xl text-uppercase">More Featires, Not Less</h3>
                                                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div>
                                            <div class="px-2 py-2">

                                                <h3 class="mb-0 sm:text-2xl text-uppercase">Everything's An Option</h3>
                                                <p class="mb-0">enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div>
                                            <div class="px-2 py-2">
                                                <h3 class="mb-0 sm:text-2xl text-uppercase">There Are No Rules</h3>
                                                <p class="mb-0">velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div>
                                            <div class="px-2 py-2">
                                                <h3 class="mb-0 sm:text-2xl text-uppercase">Your Browser, Your Business</h3>
                                                <p class="mb-0">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12">
                            <div class="row card-body py-8">
                                <div class="blockquote pt-10 pb-10 text-center">
                                    <p class="mb-0 text-uppercase">Pricing</p>
                                    <h3 class="mb-0 text-uppercase pt-6 display-2-wx">Choose your Package</h3>
                                </div>
                                <div class="col-4">
                                    <div class="card css-16gycmq-wht">
                                        <div class="px-2 py-2">
                                            <h3 class="mb-0 sm:text-2xl text-uppercase pb-8">Lorem</h3>
                                            <h3 class="mb-0 display-2-normal text-uppercase">Free</h3>
                                            <p class="mb-0">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card css-16gycmq-wht">
                                        <div class="px-2 py-2">
                                            <h3 class="mb-0 sm:text-2xl text-uppercase pb-8">Ipsum</h3>
                                            <h3 class="mb-0 display-2-normal text-uppercase">Free</h3>
                                            <p class="mb-0">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum

                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="card css-16gycmq-wht">
                                        <div class="px-2 py-2">
                                            <h3 class="mb-0 sm:text-2xl text-uppercase pb-8">dolor sit amet</h3>
                                            <h3 class="mb-0 display-2-normal text-uppercase">Custom</h3>
                                            <p class="mb-0">
                                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('partial.footer')
@endsection
@section('footer')
@include('partial.script')
@endsection
